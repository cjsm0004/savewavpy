## Componente de robocomp savewavpy

### Descripción del componente
Con este componente, podremos guardar información de audio como fichero .wav, hecho en python.
Esta versión del componente esta hecho para que **cualquier componente (es decir, varios a la vez)
pueda enviar audio a este componente** (con la interfaz correspondiente) y este se encargará
de guardarlo como .wav

Los .wav que se guarden, lo harán en la carpeta src

### Consideraciones a tener

Para que el este componente sea capaz de distinguir cual es el componente que le
envía el AudioStruct, es necesario que el atributo ```name ``` de este, no esté vacio
y tiene que ser distinto al mismo atributo que utilizan el resto de componentes que
envian audio a savewavpy. Con este atributo, es como nuestro componente es capaz de 
distinguir a los demás y por tanto clasificar la información.

Además de la interfaz AudioStructWAVI, este componente también se suscribe a la interfaz
AudioStructI, ya que siempre captura el audio que envia el componente ```audioPublisher```

También tenemos que este componente está hehco y funcioan para la versión de **python 2.7**

**IMPORTANTE**: Cuando enviamos infomación entre componentes hayq que tener en cuenta que Ice, por
defecto, no envia información superior a 1MB. Cuando vamos a trabajar con más información
en todos los componentes que vayan a necesitarlo, necesitamos cambiar o introducir el parámetro
de configuración **Ice.MessageSizeMax** en el fichero de configuración

    etc/config

### Interfaces necesarias para su uso

Para poder enviar audio a este componene, es necesario que el componente que lo haga
publique la interfaz AudioStructWAVI, pertenecientes a AudioStruct y cuyos ficheros
.ice y .idsl podremos encontrar en la carpeta ```interfaces``` de este componente.
Un ejemplo de fichero .cdsl del componente que quiera publicar para savewavpy sería:

``` 
import "AudioStruct.idsl";
    
    Component examplecomponent
    {
    	Communications
    	{
    		publishes AudioStructWAVI;
    	};
    	language Cpp;
    	
    };
```

###Versiones de este componente

Tenemos 2 versiones diferentes (1, 2), que nos serán útiles dependiendo de nuestras
necesidades (Podemos ver las acceder a cada versión, en su correspondiente rama):

1. La primera opción es **utilizar la versión 1 de este componente** (Versión actual).
    Esta versión permite que con una sola instancia del componente savewavpy, puedas enviar datos desde
    cualquier componente, y él se encarga de gestionar la información que le envian todos.

    El principal inconveniente de usar esta versión, es que si se le envia mucha información
    desde diferentes componentes, puede provocar un **cuello de botella** en el componente, y
    por tanto, haya problemas a la hora de gestionar los datos.
    

2. La segunda opción que tenemos es utilizar la **versión 2** del componente, en la que, a diferencia
    de las 2 versiones anteriores, en lugar de utilizar un paradigma Productor - Consumidor, hemos utilizado un
    paradigma **Cliente-Servidor**. Necesitaríamos lanzar una instancia de este componente
    que quiera guardar datos en el WAV. Para más información, consultar la rama asociada a esta versión

### Parámetros de configuración

Como cualquier componente en robocomp, la configuración principal la podemos
encontrar en

    etc/config
    
Nosotros hemos añadido otro fichero de configuración llamado ```configParams.py```
donde podremos configurar la duración (**en segundos**) que van a tener los .wav que guarde el
componente

    src/configParams.py

### Uso del componente

Para poder utilizar el componente, primero hemos de descargarlo. Vamos al directorio donde
lo hayamos descargado y accedemos a él:

    cd savewavpy

Una vez hayamos compilado, accederemos al directorio donde se encuentra el programa principal.

    cd src

Y ya podremos ejecutarlo

    sudo python savewavpy --Ice.Config=../etc/config