//******************************************************************
//
//  Generated by Carlos Jesús Serrano Martínez
//
//  File name: AudioStruct.ice
//
//******************************************************************
#ifndef ROBOCOMPAUDIOSTRUCT_ICE
#define ROBOCOMPAUDIOSTRUCT_ICE

module RobocompAudioStruct{

	sequence <float> Samples;
	sequence <Samples> Matrix;

	struct AudioStruct{
		
		string name;
		Matrix audioBuffer;
		string timestamp;
		int numChannels;
		float sampleRate;
		float captureTime;
		int bufferFrames;
		
	};

	interface AudioStructI{
		void publishAudioStruct (AudioStruct audioStr);
	};

	interface AudioStructVADI{
		void publishAudioStructVAD (AudioStruct audioStr);	
	};
	
	interface AudioStructDSBI{
		void setBeamAngle (float beamAngle);
		void publishAudioStructDSB (AudioStruct audioStr);
	};

	interface AudioStructWAVI{
		void saveAsWAV (AudioStruct audioStruct);
	};

};
#endif
