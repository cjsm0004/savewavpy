# -*- coding: utf-8 -*-

#wavDuration: Duración del fichero wav a guardar. Este parámetro establece la duración que tendrán
#los ficheros .wav que guardemos con este componente (en segundos). Cuando se tengan los
#suficientes datos como para guardar un .wav con esta duración, lo guardaremos, y los siguientes
#datos que recibamos, lo guardaremos, pero seguiremos recibiendo más datos hasta volver a tener
#los suficientes para otro .wav

CONFIG_PARAMS = {
    'wavDuration': 10.0
}