# -*- coding: utf-8 -*-
import numpy as np
from scipy.io import wavfile
import configParams as conf


class Data():

    def __init__(self):
        '''
        Constructor de la clase Data, donde tenemos la estructura de datos encargada de almacenar los
        datos de audio (dataStruct) hasta que sean guardados en wav
        '''
        self.dataStruct = {}
        self.wavDuration = conf.CONFIG_PARAMS['wavDuration']

    def save_data(self, audioStruct):
        """
        Funcion con la que insertamos un AudioStruct en nuestro mapa de datos
 * 		- Si ya tenemos en el mapa, una estructura de audio enviada por el
 * 		  componente del que proviene el AudioStruct, concatenamos el recibido
 * 		  junto con el que tenemos en el mapa
 * 		- En el caso de no tenes datos previos de audio de ese componente, lo almacenamos
 * 		  como un nuevo AudioStruct en nuestro mapa

         Al terminar esto, comprobamos si tenemos los datos suficientes como para guardar
         el fichero como wav. Si es así, lo guardamos como wav y borramos los datos de nuestro diccionario
         de datos
        :param audioStruct: estructura de audio, con datos de una captura de audio
        :return:
        """
        if audioStruct.name in self.dataStruct:
            print ('Concatenamos estructuras')
            astruct = self.dataStruct[audioStruct.name]
            self.concetenate_structs(astruct, audioStruct)

        else:
            print ('Insertamos nueva estructura en el mapa')
            self.dataStruct[audioStruct.name] = audioStruct

        astruct = self.dataStruct[audioStruct.name]
        if astruct.captureTime >= self.wavDuration:
            print ('Guardamos estructura en wav')
            self.save_wav(astruct)
            self.dataStruct.pop(astruct.name)
            pass

    def concetenate_structs(self, audioStruct1, audioStruct2):
        """
        Función con la que concatenamos 2 AudioStruct. La resultante
         se almacena en audioStruct1. Por concatenar entedemos:
 * 		- Juntar sus buffer de audio en 1 solo
 * 		- sumar el tiempo de captura de ambos buffer
        :param audioStruct1: Primera estructura de audio
        :param audioStruct2: Segunda estructura de audio
        :return:
        """
        audioStruct1.audioBuffer = np.concatenate((audioStruct1.audioBuffer, audioStruct2.audioBuffer), axis=1)
        audioStruct1.captureTime += audioStruct2.captureTime

    def save_wav(self, audioStruct='none'):
        """
        Función que nos permite guardar los datos de un AudioStruct concreto en un .wav.
        Como recibimos un audioStruct, primero tenemos que calcular la traspuesta del buffer
        de audio, puesto que es de dimension (numChannels, numSamples) y para guardar en wav
        necesitamos que sea (numSamples, numChannels)
        :param audioStruct: Estructura que contiene los datos que vamos a guardar en wav
        :return:
        """

        def save_struct_in_wav(audioStr):
            #print ('Guardamos estructura en wav')
            #self.debug_datastruct(audioStruct)                                              #Mostramos la estructura que vamos a guardar en el wav

            wavname = str(audioStr.name + audioStr.timestamp)                         #Formamos el nombre que va a tener el wav (nombre de la estructura + timestamp)
            audioBuffer_transpose = np.transpose(audioStr.audioBuffer)                   #Para guardar en wav, necesitamos hacer la traspuesta del buffer de audio, puesto que nosotros lo almacenamos
                                                                                            #con dimensión (numChannels, numSamples) y para guardarlo en el wav es necesario que sea (numSamples, numChannels)
            audioBuffer_transpose = np.float32(audioBuffer_transpose)                       #Pasamos los datos para trabajar con float
            wavfile.write(wavname, audioStr.sampleRate, audioBuffer_transpose)           #Guardamos datos en el wav

        if audioStruct != 'none':
            save_struct_in_wav(audioStruct)                                                 #Si es una estructura concreta la que tenemos que guardar, guardamos esa estructura
        else:
            for x, y in self.dataStruct.items():                                          #Si no nos pasan una estructura concreta, guardamos todas las que contiene datastruct
                save_struct_in_wav(y)


    def debug_datastruct(self, audioStr = 'none'):
        '''
        Función para debugear el comportamiento de datastruct. Si le pasamos por parámetro, alguna
        estructura de audio concreta, nos mostrará los datos de esa. En cambio, si no le pasamos nada por
        parámetro, nos mostrará los datos del diccionario datastruct
        :param audioStr:
        :return:
        '''

        def sout_data(audiostruct):
            print ('El nombre es :' + str(audioStr.name))
            print ('Tmaño buffer: ' + str(audiostruct.audioBuffer.shape))
            print ('Tiempo captura: ' + str(audiostruct.captureTime))
            print ('Frecuencia muestreo: ' + str(audiostruct.sampleRate))
            print ('Channels: ' + str(audiostruct.numChannels))
            print ('bufferFrames: ' + str(audiostruct.bufferFrames))

        if audioStr != 'none':
            sout_data(audioStr)
        else:
            for x, y in self.dataStruct.items():
                sout_data(y)

    def __del__(self):

        print('Guardamos todos los datos de datastruct')
        self.save_wav()

