#
# Copyright (C) 2019 by YOUR NAME HERE
#
#    This file is part of RoboComp
#
#    RoboComp is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RoboComp is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RoboComp.  If not, see <http://www.gnu.org/licenses/>.
#

import sys, os, traceback, time

from PySide import QtGui, QtCore
from genericworker import *

# If RoboComp was compiled with Python bindings you can use InnerModel in Python
# sys.path.append('/opt/robocomp/lib')
# import librobocomp_qmat
# import librobocomp_osgviewer
# import librobocomp_innermodel

from data import Data
import numpy as np

class SpecificWorker(GenericWorker):
	def __init__(self, proxy_map):
		super(SpecificWorker, self).__init__(proxy_map)
		self.timer.timeout.connect(self.compute)
		self.Period = 2000
		self.timer.start(self.Period)

		self.data = Data()				#Objeto de la clase Data que nos va a permitir almacenar los diferentes datos

	def setParams(self, params):

		return True

	@QtCore.Slot()
	def compute(self):
		##print 'SpecificWorker.compute...'


		return True


	#
	# publishAudioStruct
	#
	def publishAudioStruct(self, audioStr):
		#
		#subscribesToCODE
		#
		print ('Recibiendo estructura')
		audioStr.audioBuffer = np.array(audioStr.audioBuffer)			#Pasamos el buffer de audio a un array de numPy
		self.data.save_data(audioStr)



	#
	# saveAsWAV
	#
	def saveAsWAV(self, audioStruct):
		#
		#subscribesToCODE
		#
		print ('Recibiendo estructura')
		audioStruct.audioBuffer = np.array(audioStruct.audioBuffer)  # Pasamos el buffer de audio a un array de numPy
		self.data.save_data(audioStruct)

	def __del__(self):
		print ('Destruyendo specificworker')


